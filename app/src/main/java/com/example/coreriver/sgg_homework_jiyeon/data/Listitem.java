package com.example.coreriver.sgg_homework_jiyeon.data;

public class Listitem {

    private String title;
    private String url;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageURL() {
        return url;
    }

    public void setImageURL(String url) {
        this.url = url;
    }
}
