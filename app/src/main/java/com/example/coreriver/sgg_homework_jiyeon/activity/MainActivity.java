package com.example.coreriver.sgg_homework_jiyeon.activity;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;

import com.example.coreriver.sgg_homework_jiyeon.R;
import com.example.coreriver.sgg_homework_jiyeon.adapter.RecyclerAdapter;
import com.example.coreriver.sgg_homework_jiyeon.data.Listitem;
import com.example.coreriver.sgg_homework_jiyeon.fragment.fragment_main;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerAdapter adapter;

    fragment_main frag1;

    private ArrayList<Listitem> mItemList;
    private JSONArray jsonArray;

    private String REQUEST_URL = "http://static.ssgcdn.com/ui/app/test/subject01.json";
    private int LOAD_SUCCESS = 200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();

        getData();
        frag1 = new fragment_main(); //프래그먼트 객채셍성

        setFrag(0);
    }

    private void init() {
        RecyclerView recyclerView = findViewById(R.id.recyclerView);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        adapter = new RecyclerAdapter();
        recyclerView.setAdapter(adapter);
    }

    private void getData() {


        // 임의의 데이터입니다.
//        List<String> listTitle = Arrays.asList("국화", "사막", "수국", "해파리", "코알라", "등대", "펭귄", "튤립",
//                "국화", "사막", "수국", "해파리", "코알라", "등대", "펭귄", "튤립");
//
//        List<Integer> listResId = Arrays.asList(
//                R.drawable.ic_launcher_background,
//                R.drawable.ic_launcher_background,
//                R.drawable.ic_launcher_background,
//                R.drawable.ic_launcher_background,
//                R.drawable.ic_launcher_background,
//                R.drawable.ic_launcher_background,
//                R.drawable.ic_launcher_background,
//                R.drawable.ic_launcher_background,
//                R.drawable.ic_launcher_background,
//                R.drawable.ic_launcher_background,
//                R.drawable.ic_launcher_background,
//                R.drawable.ic_launcher_background,
//                R.drawable.ic_launcher_background,
//                R.drawable.ic_launcher_background,
//                R.drawable.ic_launcher_background,
//                R.drawable.ic_launcher_background
//        );
        for (int i = 0; i < mItemList.size(); i++) {
            // 각 List의 값들을 data 객체에 set 해줍니다.
            Listitem item_data = new Listitem();
            item_data.setTitle(mItemList.get(i).getTitle());
            item_data.setImageURL(mItemList.get(i).getImageURL());

            // 각 값이 들어간 data를 adapter에 추가합니다.
            adapter.addItem(item_data);
        }

        // adapter의 값이 변경되었다는 것을 알려줍니다.
        adapter.notifyDataSetChanged();
    }


    public void setFrag(int n){    //프래그먼트를 교체하는 작업을 하는 메소드를 만들었습니다
        FragmentManager fm = getFragmentManager();
        FragmentTransaction tran = fm.beginTransaction();
        switch (n){
            case 0:
                tran.replace(R.id.fragment, frag1);  //replace의 매개변수는 (프래그먼트를 담을 영역 id, 프래그먼트 객체) 입니다.
                tran.commit();
                break;
            case 1:
                tran.replace(R.id.fragment, frag1);  //replace의 매개변수는 (프래그먼트를 담을 영역 id, 프래그먼트 객체) 입니다.
                tran.commit();
                break;
            case 2:
                tran.replace(R.id.fragment, frag1);  //replace의 매개변수는 (프래그먼트를 담을 영역 id, 프래그먼트 객체) 입니다.
                tran.commit();
                break;
        }
    }

    private final MyHandler mHandler = new MyHandler(this);


    private class MyHandler extends Handler {
        private final WeakReference<MainActivity> weakReference;

        public MyHandler(MainActivity mainactivity) {
            weakReference = new WeakReference<MainActivity>(mainactivity);
        }

        @Override
        public void handleMessage(Message msg) {

            MainActivity mainactivity = weakReference.get();

            if (mainactivity != null) {
                switch (msg.what) {

                    case 200:
                        mainactivity.adapter.notifyDataSetChanged();
                        break;
                }
            }
        }
    }

    public void getJSON(){


        Thread thread=new Thread(new Runnable(){

            public void run(){

                String result;

                try{

                    URL url=new URL(REQUEST_URL);
                    HttpURLConnection httpURLConnection=(HttpURLConnection)url.openConnection();


                    httpURLConnection.setReadTimeout(3000);
                    httpURLConnection.setConnectTimeout(3000);
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setDoInput(true);
                    httpURLConnection.setRequestMethod("GET");
                    httpURLConnection.setUseCaches(false);
                    httpURLConnection.connect();


                    int responseStatusCode=httpURLConnection.getResponseCode();

                    InputStream inputStream;
                    if(responseStatusCode==HttpURLConnection.HTTP_OK){

                        inputStream=httpURLConnection.getInputStream();
                    }else{
                        inputStream=httpURLConnection.getErrorStream();

                    }


                    InputStreamReader inputStreamReader=new InputStreamReader(inputStream,"UTF-8");
                    BufferedReader bufferedReader=new BufferedReader(inputStreamReader);

                    StringBuilder sb=new StringBuilder();
                    String line;


                    while((line=bufferedReader.readLine())!=null){
                        sb.append(line);
                    }

                    bufferedReader.close();
                    httpURLConnection.disconnect();

                    result=sb.toString().trim();


                }catch(Exception e){
                    result=e.toString();
                }


                if(jsonParser(result)){

                    Message message=mHandler.obtainMessage(LOAD_SUCCESS);
                    mHandler.sendMessage(message);
                }
            }

        });
        thread.start();
    }

    public boolean jsonParser(String jsonString){


        if (jsonString == null ) return false;

        jsonString = jsonString.replace("jsonFlickrApi(", "");
        jsonString = jsonString.replace(")", "");

        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            JSONObject tabList = jsonObject.getJSONObject("tabList");
            JSONArray tabName = tabList.getJSONArray("tabName");
            JSONArray swipeImage = tabList.getJSONArray("swipeImage");
            JSONArray category = tabList.getJSONArray("category");
            mItemList.clear();

            for (int i = 0; i < tabList.length(); i++) {
                JSONObject photoInfo = jsonArray.getJSONObject(i);

                String name = photoInfo.getString("name");
                String url = photoInfo.getString("image");


                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String name = jsonObject.getString("color");
                String imageurl = jsonObject.getString("number");
                Listitem listitem = new Listitem();
                listitem.setTitle(name);
                listitem.setImageURL(imageurl);
                mItemList.add(listitem);

                mItemList.add(photoinfoMap);

            }

            return true;
        } catch (JSONException e) {
        }

        return false;
    }

}
